import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''
    df['Title'] = df['Name'].str.extract(' ([A-Za-z]+)\.')
    titles_to_fill = ["Mr", "Mrs", "Miss"]
    results = []

    for title in titles_to_fill:
        median_age = df.loc[df['Title'] == title, 'Age'].median()
        missing_values_count = df.loc[(df['Title'] == title) & df['Age'].isnull()].shape[0]
        median_age = round(median_age)
        results.append((title + ".", missing_values_count, median_age))
        
    return results
